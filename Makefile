################################
#Makefile for Sumo|BOT
################################

CC = gcc
SRC_DIR = src/SumoBOT/
BIN_DIR = bin/
GUI_DIR = gui/
SDL_CONFIG = `sdl-config --cflags --libs`
GLIB_CONFIG = `pkg-config --cflags --libs glib-2.0`
GMODULE_CONFIG = `pkg-config --cflags --libs gmodule-2.0`
SUMOUY_CONFIG = `pkg-config --cflags --libs sumouy`
FLAGS = -DSUMOBOT_UI_NCURSES -DSUMOBOT_UI_SDL

all:
	#Compile program
	echo "Compiling Sumo|BOT..."
	$(CC) $(SDL_CONFIG) $(GLIB_CONFIG) $(GMODULE_CONFIG) $(SUMOUY_CONFIG) -o $(BIN_DIR)sumobot $(FLAGS) -rdynamic -ldl -lncurses -lpthread -lSDL_gfx -lSDL_ttf -lSDL_image $(SRC_DIR)sumobot.c $(SRC_DIR)ncursesinterface.c $(SRC_DIR)sdlinterface.c $(SRC_DIR)ai.c $(SRC_DIR)debug.c	
	echo "Copying GUI files..."
	mkdir -p $(BIN_DIR)sdl
	cp $(GUI_DIR)* $(BIN_DIR)sdl	
clean:
	#Cleans compiling thrash
	echo "Cleaning thrash..."
	rm -f -r $(BIN_DIR)*