/*
    Sumo|BOT - Client program for use in the Sumo|UY robotic fight events.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Sumo|BOT.

    Sumo|BOT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Sumo|BOT (2009 - Steven Rodriguez -)                                 //
//=====================================================================//
#if !defined(SUMOBOT_UI_NCURSES) && !defined(SUMOBOT_UI_SDL)

#error "Please, compile with a least one interface"

#endif
//===========================================================//
//Libraries                                                  //
//===========================================================//

#include "sumobot.h"

#if defined(SUMOBOT_UI_NCURSES)

#include "ncursesinterface.h"

#endif

#if defined(SUMOBOT_UI_SDL)

#include "sdlinterface.h"

#endif

//===========================================================//
//Internal Variables                                         //
//===========================================================//

//Configuration specific
static char *debugfile;
static struct Vector2 dohyocenter;
static SumoUY_Int32 dohyoradius;

//Sumo|LIB specific
static struct SumoBot *bot;
static struct NetworkAddress botaddress;
static struct NetworkAddress serveraddress;

//UI specific
#if defined(SUMOBOT_UI_NCURSES)
static SumoUY_UInt8 ncurses;
#endif
#if defined(SUMOBOT_UI_SDL)
static SumoUY_UInt8 sdl;
#endif
static SumoUY_UInt32 fps;
static SumoUY_Int8 fullscreen;
static SumoUY_UInt32 width;
static SumoUY_UInt32 height;
static SumoUY_UInt8 bpp;
static SumoUY_UInt8 usejoystick;

//AI specific
static char *aimodule;

//Command line arguments
static GOptionEntry entries[] =
{
    {"debugfile", 'd', 0, G_OPTION_ARG_STRING, &debugfile,"Debug filename", "FILE"},
    {"dohyo-center-X", 'x', 0, G_OPTION_ARG_INT, &dohyocenter.X,"Dohyo center (X coordinate)", "INT"},
    {"dohyo-center-y", 'y', 0, G_OPTION_ARG_INT, &dohyocenter.Y,"Dohyo center (Y coordinate)", "INT"},
    {"dohyo-radius", 'r', 0, G_OPTION_ARG_INT, &dohyoradius,"Dohyo radius", "INT"},
    {"bot-host", 'h', 0, G_OPTION_ARG_STRING, &botaddress.Host,"Bot host", "STRING"},
    {"bot-port", 'p', 0, G_OPTION_ARG_INT, &botaddress.Port,"Bot port", "INT"},
    {"server-host", 'k', 0, G_OPTION_ARG_STRING, &serveraddress.Host,"Server host", "STRING"},
    {"server-port", 'l', 0, G_OPTION_ARG_INT, &serveraddress.Port,"Server port", "INT"},
    {"frames-per-second", 'f', 0, G_OPTION_ARG_INT, &fps,"Frames per second", "INT"},
    {"fullscreen", 'g', 0, G_OPTION_ARG_NONE, &fullscreen,"Fullscreen", NULL},
    {"screen-width", 'w', 0, G_OPTION_ARG_INT, &width,"Screen width", "INT"},
    {"screen-height", 'e', 0, G_OPTION_ARG_INT, &height,"Screen height", "INT"},
    {"bits-per-pixel", 'b', 0, G_OPTION_ARG_INT, &bpp,"Bits per pixel", "INT"},
    {"use-joystick", 'j', 0, G_OPTION_ARG_NONE, &usejoystick,"Use joystick", NULL},
    {"ai-module-file", 'a', 0, G_OPTION_ARG_STRING, &aimodule,"AI module file", "FILE"},
#if defined(SUMOBOT_UI_NCURSES)
    {"ncurses", 'n', 0, G_OPTION_ARG_NONE, &ncurses,"Ncurses interface", NULL},
#endif
#if defined(SUMOBOT_UI_SDL)
    {"sdl", 's', 0, G_OPTION_ARG_NONE, &sdl,"SDL interface", NULL},
#endif
    {NULL}
};
static GOptionContext *context;

//Interface
static struct SumoBOTInterface interface;

//===========================================================//
//Functions                                                  //
//===========================================================//

int main(int argc, char *argv[])
{
    //Parse parameters
    context = g_option_context_new("- Sumo|BOT client program for the Sumo|UY competition.");

    g_option_context_add_main_entries(context, entries, "GETTEXT_PACKAGE");
    g_option_context_set_help_enabled(context, TRUE);

    if(g_option_context_parse(context, &argc, &argv, NULL) == FALSE)
    {
        printf("%s", g_option_context_get_help(context, TRUE, NULL));
        g_option_context_free(context);
        return 0;
    }

    //Fill the restant parameters
    if(dohyoradius == 0)
        dohyoradius = 750;
    if(botaddress.Host == NULL)
        botaddress.Host = "127.0.0.1";
    if(botaddress.Port == 0)
        botaddress.Port = 7001;
    if(serveraddress.Host == NULL)
        serveraddress.Host = "127.0.0.1";
    if(serveraddress.Port == 0)
        serveraddress.Port = 8001;
    if(fps == 0)
        fps = 30;
    if(width == 0)
        width = 640;
    if(height == 0)
        height = 480;
    if(bpp == 0)
        bpp = 24;

    //Initialize debug system
    if(debugfile != NULL)
    {
        if(Debug_Load(WriteInFile, debugfile) == SUMOUY_FALSE)
        {
            printf("Could not load debug system");
            return 0;
        }
    }
    else
    {
        if(Debug_Load(WriteInEvent, NULL) == SUMOUY_FALSE)
        {
            printf("Could not load debug system");
            return 0;
        }

        Debug_SetEvent(WriteDebugMessage);
    }

    //Initialize Sumo|LIB
    if(SumoUY_Init() == SUMOUY_FALSE)
    {
        Debug_Write(SumoUY_GetErrorString());
        Debug_Unload();
        g_option_context_free(context);
        SumoUY_Close();
        return 0;
    }

    //Create bot and check if creates
    bot = SumoUY_CreateSumoBot(serveraddress, botaddress);

    if(bot == NULL)
    {
        Debug_Write(SumoUY_GetErrorString());
        Debug_Unload();
        g_option_context_free(context);
        SumoUY_Close();
        return 0;
    }

    //Initialize AI module
    if(aimodule != NULL)
    {
        if(AI_LoadModule(aimodule, bot, dohyocenter, dohyoradius, Debug_Write, SUMOUY_FALSE) == SUMOUY_FALSE)
        {
            Debug_Write("Can't Load the AI module, interface will be without AI modules");
        }
    }

    //Load the UI
#if defined(SUMOBOT_UI_NCURSES) && defined(SUMOBOT_UI_SDL)
    if((ncurses == SUMOUY_TRUE && sdl == SUMOUY_TRUE) || (ncurses == SUMOUY_FALSE && sdl == SUMOUY_FALSE))
    {
        Debug_Write("Please, select an interface");
        if(AI_IsLoaded() == SUMOUY_TRUE)
            AI_UnloadModule();
        Debug_Unload();
        g_option_context_free(context);
        SumoUY_Close();
        return 0;
    }
#endif
#if defined(SUMOBOT_UI_NCURSES)
    if(ncurses == SUMOUY_TRUE)
    {
        interface.LoadInterface = NCurses_LoadInterface;
    }
#endif
#if defined(SUMOBOT_UI_SDL)
    if(sdl == SUMOUY_TRUE)
    {
        interface.LoadInterface = SDL_LoadInterface;
    }
#endif
    //Show the interface
    interface.LoadInterface(bot, dohyocenter, dohyoradius, fps, fullscreen, width, height, bpp, usejoystick);

    //Close the Sumo|LIB , AI system, debug system and the context
    if(AI_IsLoaded() == SUMOUY_TRUE)
        AI_UnloadModule();
    Debug_Unload();
    g_option_context_free(context);
    SumoUY_Close();

    return 0;
}

void WriteDebugMessage(char *message)
{
    printf("%s\n", message);
}
