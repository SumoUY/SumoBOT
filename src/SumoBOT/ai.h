/*
    Sumo|BOT - Client program for use in the Sumo|UY robotic fight events.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Sumo|BOT.

    Sumo|BOT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Sumo|BOT (2009 - Steven Rodriguez -)                                 //
//=====================================================================//

////////////////////////////////////////////////////////////////////
/// \file ai.h
/// \brief Sumo|BOT AI file.
/// \details This is the AI module system of the Sumo|BOT program.
////////////////////////////////////////////////////////////////////

//===========================================================//
//Libraries                                                  //
//===========================================================//

#if !defined(SUMOBOTHEADER)

#include "sumobot.h"

#endif

//===========================================================//
//Functions                                                  //
//===========================================================//

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 AI_LoadModule(char *fileName, struct SumoBot *bot, struct Vector2 dohyoCenter, SumoUY_UInt32 dohyoRadius, void (*onMessage)(SumoUY_UInt8 type, char *message), SumoUY_UInt8 aiActive)
/// \brief Loads the AI module specified.
/// \param fileName The module file to load.
/// \param bot The bot to use.
/// \param dohyoCenter The center of the dohyo.
/// \param dohyoRadius The radius of the dohyo.
/// \param onMessage The function to post messages.
/// \param aiActive SUMOUY_TRUE if Sumo|BOT AI system is active or SUMOUY_FALSE if not.
/// \return SUMOUY_TRUE if loads or SUMOUY_FALSE if not.
////////////////////////////////////////////////////
SumoUY_UInt8 AI_LoadModule(char *fileName, struct SumoBot *bot, struct Vector2 dohyoCenter, SumoUY_UInt32 dohyoRadius, void (*onMessage)(char *message), SumoUY_UInt8 aiActive);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 AI_UnloadModule()
/// \brief Unloads the AI module.
/// \return SUMOUY_TRUE if unloads or SUMOUY_FALSE if not.
////////////////////////////////////////////////////
SumoUY_UInt8 AI_UnloadModule();

////////////////////////////////////////////////////
/// \fn char *AI_GetModuleName()
/// \brief Gets the AI module name.
/// \return The module name or NULL if there is an error.
////////////////////////////////////////////////////
char *AI_GetModuleName();

////////////////////////////////////////////////////
/// \fn char *AI_GetModuleVersion()
/// \brief Gets the AI module version.
/// \return The module version or NULL if there is an error.
////////////////////////////////////////////////////
char *AI_GetModuleVersion();

////////////////////////////////////////////////////
/// \fn char *AI_GetModuleVendor()
/// \brief Gets the AI module vendor.
/// \return The module vendor or NULL if there is an error.
////////////////////////////////////////////////////
char *AI_GetModuleVendor();

////////////////////////////////////////////////////
/// \fn void AI_ProcessStep()
/// \brief Do a process iteration on the AI system.
////////////////////////////////////////////////////
void AI_ProcessStep();

////////////////////////////////////////////////////
/// \fn void AI_SendAIToggleEvent(SumoUY_UInt8 activated)
/// \brief Sends to the module the state of the Sumo|BOT AI system.
/// \param activated SUMOUY_TRUE if the AI system is active and SUMOUY_FALSE if the AI system is not active.
////////////////////////////////////////////////////
void AI_SendAIToggleEvent(SumoUY_UInt8 activated);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 AI_IsLoaded()
/// \brief Verifies if the AI module is loaded.
/// \return SUMOUY_TRUE if loaded and SUMOUY_FALSE if not.
////////////////////////////////////////////////////
SumoUY_UInt8 AI_IsLoaded();
