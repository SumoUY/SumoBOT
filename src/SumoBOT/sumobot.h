/*
    Sumo|BOT - Client program for use in the Sumo|UY robotic fight events.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Sumo|BOT.

    Sumo|BOT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Sumo|BOT (2009 - Steven Rodriguez -)                                 //
//=====================================================================//

////////////////////////////////////////////////////////////////////
/// \file sumobot.h
/// \brief Sumo|BOT header file.
/// \details This is the header file for use in the Sumo|BOT program.
////////////////////////////////////////////////////////////////////

//===========================================================//
//Libraries                                                  //
//===========================================================//

#if !defined(SUMOBOTHEADER)

#define SUMOBOTHEADER

#include <sumouy.h>
#include <gmodule.h>
#include <glib/gstdio.h>
#include "debug.h"
#include "ai.h"

#endif

//===========================================================//
//Enumerations                                               //
//===========================================================//

enum DebugType
{
    WriteInFile = 0,
    WriteInEvent = 1
};

//===========================================================//
//Structures                                                 //
//===========================================================//

struct SumoBOTInterface
{
    void (*LoadInterface)(struct SumoBot *bot, struct Vector2 dohyoCenter, SumoUY_UInt32 dohyoRadius, SumoUY_UInt32 fps, SumoUY_Int8 fullscreen, SumoUY_UInt32 width, SumoUY_UInt32 height, SumoUY_UInt8 bpp, SumoUY_UInt8 useJoystick);
};

struct AIModule
{
    char *Name;
    char *Version;
    char *Vendor;
    SumoUY_UInt8 (*Load)(struct SumoBot *bot, struct Vector2 dohyoCenter, SumoUY_UInt32 dohyoRadius, void (*onMessage)(char *message), SumoUY_UInt8 aiactive);
    void (*ProcessStep)();
    void (*OnSumoBotAIToggle)(SumoUY_UInt8 activated);
    SumoUY_UInt8 (*Close)();
};

//===========================================================//
//Functions                                                  //
//===========================================================//

////////////////////////////////////////////////////
/// \fn void WriteDebugMessage(char *message)
/// \brief Writes a message in the debug system.
/// \param message The message to write.
////////////////////////////////////////////////////
void WriteDebugMessage(char *message);
