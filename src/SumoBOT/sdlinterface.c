/*
    Sumo|BOT - Client program for use in the Sumo|UY robotic fight events.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Sumo|BOT.

    Sumo|BOT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Sumo|BOT (2009 - Steven Rodriguez -)                                 //
//=====================================================================//
#if defined(SUMOBOT_UI_SDL)
//=========================================//
//Libraries                                //
//=========================================//

#include "sdlinterface.h"

//=========================================//
//Internal variables                       //
//=========================================//

//SDL specific
static SDL_Surface *screen;
static SDL_Event events;
static GString *text;

//TTF specific
static TTF_Font *font;

//Elements
static SDL_Surface *dohyo;
static SDL_Surface *mainbot;
static SDL_Surface *opponentbot;
static SDL_Surface *newmainbot;
static SDL_Surface *newopponentbot;
static SDL_Surface *logo;

//UI specific
static SumoUY_UInt32 framespersecond;
static SumoUY_UInt8 finish;
static SumoUY_UInt8 hasjoystick;
static SDL_Joystick *joystick;
static SumoUY_Int8 currentspeed;
static SumoUY_UInt8 ai;
static struct Vector2 dohyoposition;

//Fps calculation
static SumoUY_UInt32 frametime;
static SumoUY_UInt32 starttime;
static SumoUY_UInt32 endtime;
static SumoUY_Int32 totaltime;
static SumoUY_UInt32 actualfps;

//Debug specific
static GString *debugstring;

//Sumo|LIB specific
static struct SumoBot *sumobot;
static struct Vector2 dcenter;
static SumoUY_UInt32 dradius;
static struct BotSpeed lastspeed;
static SumoUY_UInt16 previousrotation;
static SumoUY_UInt16 actualrotation;

//=========================================//
//Functions                                //
//=========================================//

void SDL_LoadInterface(struct SumoBot *bot, struct Vector2 dohyoCenter, SumoUY_UInt32 dohyoRadius, SumoUY_UInt32 fps, SumoUY_Int8 fullscreen, SumoUY_UInt32 width, SumoUY_UInt32 height, SumoUY_UInt8 bpp, SumoUY_UInt8 useJoystick)
{
    SumoUY_UInt32 flags = SDL_HWSURFACE | SDL_DOUBLEBUF;

    //Initialize variables
    debugstring = g_string_new(NULL);
    currentspeed = 0;
    ai = SUMOUY_FALSE;
    lastspeed = SumoUY_CreateBotSpeed(0, 0);

    //Obtain variables
    sumobot = bot;
    dcenter = dohyoCenter;
    dradius = dohyoRadius;
    framespersecond = fps;
    hasjoystick = useJoystick;

	//Initializes SDL and check for errors
	if(hasjoystick == SUMOUY_TRUE)
	{
		if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_TIMER) == -1)
        {
            Debug_Write("Could not init SDL");
			g_string_free(debugstring, TRUE);
			return;
        }

         //Check if number of joysticks is >= 1
		if(SDL_NumJoysticks() == 0)
		{
			Debug_Write("Joystick not found. Please, connect a joystick");
			g_string_free(debugstring, TRUE);
			return;
		}

        //Open joystick
		joystick = SDL_JoystickOpen(0);

		if(joystick == NULL)
		{
			Debug_Write("Can't open joystick");
			g_string_free(debugstring, TRUE);
			return;
		}

		//Select joystick to update manually
		SDL_JoystickEventState(SDL_IGNORE);
	}
	else
	{
		if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) == -1)
        {
            Debug_Write("Could not init SDL");
			g_string_free(debugstring, TRUE);
			return;
        }
	}

	if(fullscreen == SUMOUY_TRUE)
	{
		flags |= SDL_FULLSCREEN;
	}

	//Initializes TTF and check for errors
	if(TTF_Init() == -1)
	{
		Debug_Write("Could not initialize TTF system");
		return;
	}

	//Opens a font
	font = TTF_OpenFont("sdl/font.ttf",height / 30);

	if(font == NULL)
	{
		Debug_Write("Could not create font");
		return;
	}

	//Sets the window caption
	SDL_WM_SetCaption("Sumo|BOT (2009 - Steven Rodriguez)",NULL);

	//Sets the window icon
	if(g_file_test("sdl/icon.png", G_FILE_TEST_EXISTS) == TRUE)
	{
            SDL_WM_SetIcon(IMG_Load("sdl/icon.png"), NULL);
	}
	else
	{
            Debug_Write("Can't load icon");
	}

	//Initializes the vidoe screen
	screen = SDL_SetVideoMode(width, height, bpp, flags);

	if(screen == NULL)
	{
		Debug_Write("Could not create screen");
		return;
	}

	//Create the sufrace of elements
	dohyo = SDL_CreateDohyo((height / 2) / 2);
	mainbot = SDL_CreateBot((((height / 2) / 2) * 14) / 100,(((height / 2) / 2) * 14) / 100,SUMOUY_FALSE);
	opponentbot = SDL_CreateBot((((height / 2) / 2) * 14) / 100,(((height / 2) / 2) * 14) / 100,SUMOUY_TRUE);
	newmainbot = SDL_CreateBot((((height / 2) / 2) * 14) / 100,(((height / 2) / 2) * 14) / 100,SUMOUY_FALSE);
	newopponentbot = SDL_CreateBot((((height / 2) / 2) * 14) / 100,(((height / 2) / 2) * 14) / 100,SUMOUY_TRUE);
	logo = SDL_CreateLogo("sdl/logo.png");

	//Calculate dohyo position
	dohyoposition.X = width - (((height - dohyo->h) / 2) + (dohyo->h / 2));
	dohyoposition.Y = ((height - dohyo->h) / 2) + (dohyo->h / 2);

	if(dohyo == NULL || mainbot == NULL || opponentbot == NULL || logo == NULL)
	{
        Debug_Write("There is an error creating the surfaces");
		return;
	}

	//Initialize text
	text = g_string_new(NULL);

	//Change debug function
    Debug_SetEvent(SDL_Debug);

    //Initialize the frame limiter
    frametime = 1000 / framespersecond;

    //Initialize Bot
    SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(0, 0));

	//SDL main loop
    finish = SUMOUY_FALSE;

    while(finish == SUMOUY_FALSE)
    {
        //----------------------------
		//Frame limiter function
		starttime = SDL_GetTicks();
		//----------------------------

        //Check for errors
        if(SumoUY_GetError() != SUMOUY_NO_ERROR)
        {
                Debug_Write(SumoUY_GetErrorString());
                break;
        }

        //Check input
        SDL_CheckInput();

        //Process AI or update the bot state
        if (ai == SUMOUY_FALSE)
        {
            if(SumoUY_BotProcessData(bot) == SUMOUY_FALSE)
            {
                Debug_Write(SumoUY_GetErrorString());
            }
        }
        else
        {
            AI_ProcessStep();
        }

        //Show Frame
        SDL_ShowFrame();

        //----------------------------
        //Frame limiter function
        endtime = SDL_GetTicks();

        totaltime = frametime - (endtime - starttime);

		if(totaltime > 0)
		{
			SDL_Delay(frametime - (endtime - starttime));
		}

		endtime = SDL_GetTicks();
		actualfps = 1000 / (endtime - starttime);
        //----------------------------
    }

    //Unload SDL and TTF
    //==================================

    //Free surfaces
	SDL_FreeSurface(dohyo);
	SDL_FreeSurface(mainbot);
	SDL_FreeSurface(opponentbot);
	SDL_FreeSurface(logo);
	SDL_FreeSurface(newmainbot);
	SDL_FreeSurface(newopponentbot);

	//Free the text container
	g_string_free(text, TRUE);

	//Check if TTF was initialized
	if(TTF_WasInit() != -1)
	{
		TTF_CloseFont(font);
		TTF_Quit();
	}

	//Check if SDL was initialized
	if(SDL_WasInit(SDL_INIT_VIDEO) != 0 && SDL_WasInit(SDL_INIT_JOYSTICK) != 0 && SDL_WasInit(SDL_INIT_TIMER) != 0)
	{
		SDL_Quit();
	}
}

void SDL_CheckInput()
{
    if(SDL_PollEvent(&events) == 1)
    {
        //Check for video window events
		if(events.type == SDL_QUIT)
		{
		    finish = SUMOUY_TRUE;
			return;
		}

		//Check keyboard input
        if((events.type == SDL_KEYDOWN) && (hasjoystick == SUMOUY_FALSE))
        {
            switch(events.key.keysym.sym)
            {
                case SDLK_q: //Check QUIT action
                    finish = SUMOUY_TRUE;
                    return;
                case SDLK_a: //Check AI TOGGLE action
                    if(AI_IsLoaded() == SUMOUY_TRUE)
                    {
                        if(ai == SUMOUY_TRUE)
                        {
                            ai = SUMOUY_FALSE;
                        }
                        else
                        {
                            ai = SUMOUY_TRUE;
                        }
                        AI_SendAIToggleEvent(ai);
                    }
                    return;
                default:
                    break;
            }
        }

        if(ai == SUMOUY_FALSE)
        {
            //Check keyboard input
            if((events.type == SDL_KEYDOWN) && (hasjoystick == SUMOUY_FALSE))
            {
                switch(events.key.keysym.sym)
                {
                    case SDLK_0: //Check SPEED 0 action
                        currentspeed = 0;
                        return;
                    case SDLK_1: //Check SPEED 1 action
                        currentspeed = 1;
                        return;
                    case SDLK_2: //Check SPEED 2 action
                        currentspeed = 2;
                        return;
                    case SDLK_3: //Check SPEED 3 action
                        currentspeed = 3;
                        return;
                    case SDLK_4: //Check SPEED 4 action
                        currentspeed = 4;
                        return;
                    case SDLK_5: //Check SPEED 5 action
                        currentspeed = 5;
                        return;
                    case SDLK_RIGHT: //Check RIGHT action
                        if(SDL_CompareSpeeds(lastspeed, SumoUY_CreateBotSpeed(currentspeed, - currentspeed)) == SUMOUY_FALSE)
                        {
                            SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(currentspeed, - currentspeed));
                            lastspeed = SumoUY_CreateBotSpeed(currentspeed, - currentspeed);
                        }
                        SDL_CheckSumoLIBError();
                        return;
                    case SDLK_LEFT: //Check LEFT action
                        if(SDL_CompareSpeeds(lastspeed, SumoUY_CreateBotSpeed(- currentspeed, currentspeed)) == SUMOUY_FALSE)
                        {
                            SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(- currentspeed, currentspeed));
                            lastspeed = SumoUY_CreateBotSpeed(- currentspeed, currentspeed);
                        }
                        SDL_CheckSumoLIBError();
                        return;
                    case SDLK_UP: //Check UP action
                        if(SDL_CompareSpeeds(lastspeed, SumoUY_CreateBotSpeed(currentspeed, currentspeed)) == SUMOUY_FALSE)
                        {
                            SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(currentspeed, currentspeed));
                            lastspeed = SumoUY_CreateBotSpeed(currentspeed, currentspeed);
                        }
                        SDL_CheckSumoLIBError();
                        return;
                    case SDLK_DOWN: //Check DOWN action
                        if(SDL_CompareSpeeds(lastspeed, SumoUY_CreateBotSpeed(- currentspeed, - currentspeed)) == SUMOUY_FALSE)
                        {
                            SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(- currentspeed, - currentspeed));
                            lastspeed = SumoUY_CreateBotSpeed(- currentspeed, - currentspeed);
                        }
                        SDL_CheckSumoLIBError();
                        return;
                    default:
                        break;
                }

            }
            else if((events.type == SDL_KEYUP) && (hasjoystick == SUMOUY_FALSE))
            {
                //Check STOP action
                if(SDL_CompareSpeeds(lastspeed, SumoUY_CreateBotSpeed(0, 0)) == SUMOUY_FALSE)
                {
                    SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(0, 0));
                    lastspeed = SumoUY_CreateBotSpeed(0, 0);
                }
                SDL_CheckSumoLIBError();
                return;
            }
        }
    }

    //Check joystick input
    if(hasjoystick == SUMOUY_TRUE)
    {
        //Poll joystick status
		SDL_JoystickUpdate();

		//Check QUIT action
		if(SDL_JoystickGetButton(joystick,8) == 1)
		{
			finish = SUMOUY_TRUE;
            return;
		}

		//Check AI TOGGLE action
        if(SDL_JoystickGetButton(joystick,6) == 1)
        {
            if(AI_IsLoaded() == SUMOUY_TRUE)
            {
                if(ai == SUMOUY_TRUE)
                {
                    ai = SUMOUY_FALSE;
                }
                else
                {
                    ai = SUMOUY_TRUE;
                }
                AI_SendAIToggleEvent(ai);
            }
            return;
        }

		if(ai == SUMOUY_FALSE)
		{
            //Check SPEED 0 action
            if(SDL_JoystickGetButton(joystick,0) == 1)
            {
                currentspeed = 0;
                return;
            }

            //Check SPEED 1 action
            if(SDL_JoystickGetButton(joystick,1) == 1)
            {
                currentspeed = 1;
                return;
            }

            //Check SPEED 2 action
            if(SDL_JoystickGetButton(joystick,2) == 1)
            {
                currentspeed = 2;
                return;
            }

            //Check SPEED 3 action
            if(SDL_JoystickGetButton(joystick,3) == 1)
            {
                currentspeed = 3;
                return;
            }

            //Check SPEED 4 action
            if(SDL_JoystickGetButton(joystick,4) == 1)
            {
                currentspeed = 4;
                return;
            }

            //Check SPEED 5 action
            if(SDL_JoystickGetButton(joystick,5) == 1)
            {
                currentspeed = 5;
                return;
            }

            //Check STOP action
            if(SDL_JoystickGetAxis(joystick,0) == 0 && SDL_JoystickGetAxis(joystick,1) == 0)
            {
                if(SDL_CompareSpeeds(lastspeed, SumoUY_CreateBotSpeed(0, 0)) == SUMOUY_FALSE)
                {
                    SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(0, 0));
                    lastspeed = SumoUY_CreateBotSpeed(0, 0);
                }
                SDL_CheckSumoLIBError();
                return;
            }

            //Check RIGHT action
            if(SDL_JoystickGetAxis(joystick,0) > 0)
            {
                if(SDL_CompareSpeeds(lastspeed, SumoUY_CreateBotSpeed(currentspeed, - currentspeed)) == SUMOUY_FALSE)
                {
                    SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(currentspeed, - currentspeed));
                    lastspeed = SumoUY_CreateBotSpeed(currentspeed, - currentspeed);
                }
                SDL_CheckSumoLIBError();
                return;
            }

            //Check LEFT action
            if(SDL_JoystickGetAxis(joystick,0) < 0)
            {
                if(SDL_CompareSpeeds(lastspeed, SumoUY_CreateBotSpeed(- currentspeed, currentspeed)) == SUMOUY_FALSE)
                {
                    SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(- currentspeed, currentspeed));
                    lastspeed = SumoUY_CreateBotSpeed(- currentspeed, currentspeed);
                }
                SDL_CheckSumoLIBError();
                return;
            }

            //Check UP action
            if(SDL_JoystickGetAxis(joystick,1) < 0)
            {
                if(SDL_CompareSpeeds(lastspeed, SumoUY_CreateBotSpeed(currentspeed, currentspeed)) == SUMOUY_FALSE)
                {
                    SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(currentspeed, currentspeed));
                    lastspeed = SumoUY_CreateBotSpeed(currentspeed, currentspeed);
                }
                SDL_CheckSumoLIBError();
                return;
            }

            //Check DOWN action
            if(SDL_JoystickGetAxis(joystick,1) > 0)
            {
                if(SDL_CompareSpeeds(lastspeed, SumoUY_CreateBotSpeed(- currentspeed, - currentspeed)) == SUMOUY_FALSE)
                {
                    SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(- currentspeed, - currentspeed));
                    lastspeed = SumoUY_CreateBotSpeed(- currentspeed, - currentspeed);
                }
                SDL_CheckSumoLIBError();
                return;
            }
		}
    }
}

void SDL_ShowFrame()
{
    //Clears the screen
	SDL_FillRect(screen,NULL,SDL_MapRGBA(screen->format,0,0,0,255));

	//Draw dohyo elements
	if(SDL_DrawDohyo(dohyoposition.X,dohyoposition.Y) == SUMOUY_FALSE)
	{
	    Debug_Write("Can't draw dohyo.");
	    finish = SUMOUY_TRUE;
		return;
	}

	//Get actual rotation
	actualrotation = SumoUY_BotGetRotation(sumobot);
    SDL_CheckSumoLIBError();

	//Draw our bot
	if(SDL_DrawBot(SumoUY_BotGetPosition(sumobot), dradius, dcenter, dohyo->w / 2, dohyoposition, SumoUY_BotGetRotation(sumobot), SUMOUY_FALSE) == SUMOUY_FALSE)
	{
	    Debug_Write("Can't draw bot");
		return;
	}
	SDL_CheckSumoLIBError();

	//Draw enemy bot
	if(SDL_DrawBot(SumoUY_BotGetOpponentPosition(sumobot), dradius,dcenter, dohyo->w / 2, dohyoposition, SumoUY_BotGetOpponentRotation(sumobot), SUMOUY_TRUE) == SUMOUY_FALSE)
	{
	    Debug_Write("Can't draw opponent bot");
		return;
	}
	SDL_CheckSumoLIBError();

	//Draw FPS
	g_string_printf(text, "FPS: %i", actualfps);
	SDL_DrawText(text->str, 0, 0, 255, 255, 0);

	//Draw position
	g_string_printf(text, "Position: X:%i Y:%i", SumoUY_BotGetPosition(sumobot).X, SumoUY_BotGetPosition(sumobot).Y);
	SDL_CheckSumoLIBError();
	SDL_DrawText(text->str, 0, (screen->h / 30) * 1, 255, 255, 0);

	//Draw rotation
	g_string_printf(text, "Rotation: %hu", SumoUY_BotGetRotation(sumobot));
	SDL_CheckSumoLIBError();
    SDL_DrawText(text->str, 0, (screen->h / 30) * 2, 255, 255, 0);

	//Draw yukos
	g_string_printf(text, "Yukos: %hhu", SumoUY_BotGetYukos(sumobot));
	SDL_CheckSumoLIBError();
    SDL_DrawText(text->str, 0, (screen->h / 30) * 3, 255, 255, 0);

	//Draw opponent position
	g_string_printf(text, "Opponent position: X:%i Y:%i", SumoUY_BotGetOpponentPosition(sumobot).X, SumoUY_BotGetOpponentPosition(sumobot).Y);
	SDL_CheckSumoLIBError();
    SDL_DrawText(text->str, 0, (screen->h / 30) * 4, 255, 255, 0);

    //Draw opponent rotation
	g_string_printf(text, "Opponent rotation: %hu", SumoUY_BotGetOpponentRotation(sumobot));
	SDL_CheckSumoLIBError();
	SDL_DrawText(text->str, 0, (screen->h / 30) * 5, 255, 255, 0);

	//Draw opponent yukos
	g_string_printf(text, "Opponent yukos: %hhu", SumoUY_BotGetOpponentYukos(sumobot));
	SDL_CheckSumoLIBError();
    SDL_DrawText(text->str, 0, (screen->h / 30) * 6, 255, 255, 0);

	//Draw left wheel speed
	g_string_printf(text, "Left wheel speed: %i", SumoUY_BotGetSpeed(sumobot).LeftWheelSpeed);
	SDL_CheckSumoLIBError();
    SDL_DrawText(text->str, 0, (screen->h / 30) * 7, 255, 255, 0);

	//Draw right wheel speed
	g_string_printf(text, "Right wheel speed: %i", SumoUY_BotGetSpeed(sumobot).RightWheelSpeed);
	SDL_CheckSumoLIBError();
    SDL_DrawText(text->str, 0, (screen->h / 30) * 8, 255, 255, 0);

	//Draw manual speed
	g_string_printf(text, "Manual speed: %hhd", currentspeed);
	SDL_CheckSumoLIBError();
    SDL_DrawText(text->str, 0, (screen->h / 30) * 10, 255, 0, 0);

    //Draw AI module (name, version and vendor)
    if(AI_IsLoaded() == SUMOUY_TRUE)
    {
        g_string_printf(text, "AI Module: loaded");
        SDL_DrawText(text->str, 0, (screen->h / 30) * 12, 0, 255, 0);
        g_string_printf(text, "Name: %s", AI_GetModuleName());
        SDL_DrawText(text->str, 0, (screen->h / 30) * 13, 0, 255, 0);
        g_string_printf(text, "Version: v%s", AI_GetModuleVersion());
        SDL_DrawText(text->str, 0, (screen->h / 30) * 14, 0, 255, 0);
        g_string_printf(text, "Vendor: %s", AI_GetModuleVendor());
        SDL_DrawText(text->str, 0, (screen->h / 30) * 15, 0, 255, 0);
    }
    else
    {
        g_string_printf(text, "AI Module: not loaded");
        SDL_DrawText(text->str, 0, (screen->h / 30) * 12, 0, 255, 0);
    }

	//Draw AI
	if(ai == SUMOUY_TRUE)
	{
		g_string_printf(text, "AI: active");
	}
	else
	{
		g_string_printf(text, "AI: inactive");
	}

	SDL_DrawText(text->str, 0, (screen->h / 30) * 16, 0, 255, 0);

	//Debug
	g_string_printf(text, "Debug: %s", debugstring->str);
	SDL_DrawText(text->str, 0, (screen->h / 30) * 18, 255, 255, 255);

	//Draw Logo
	if(SDL_DrawLogo() == SUMOUY_FALSE)
	{
	    Debug_Write("Can't draw logo");
		return;
	}

	//Get previous rotation
	previousrotation = SumoUY_BotGetRotation(sumobot);
	SDL_CheckSumoLIBError();

	//Swap buffers
	if(SDL_Flip(screen) == -1)
	{
		Debug_Write("Can't swap buffers");
		return;
	}
}

void SDL_Debug(char *message)
{
    g_string_printf(debugstring, "%s", message);
}

void SDL_CheckSumoLIBError()
{
    if(SumoUY_GetError() != SUMOUY_NO_ERROR)
    {
        Debug_Write(SumoUY_GetErrorString());
    }
}

SumoUY_UInt8 SDL_DrawDohyo(SumoUY_Int32 x, SumoUY_Int32 y)
{
	SDL_Rect area;

	//Creates the area for drawing
	area.x = x - (dohyo->w / 2);
	area.y = y - (dohyo->h / 2);
	area.w = x + (dohyo->w / 2);
	area.h = y + (dohyo->h / 2);

	//Draws the dohyo to screen surface
	if (SDL_BlitSurface(dohyo,NULL,screen,&area) == -1)
	{
		return SUMOUY_FALSE;
	}

	return SUMOUY_TRUE;
}

SumoUY_UInt8 SDL_DrawText(char *text,SumoUY_UInt32 x, SumoUY_UInt32 y, SumoUY_UInt8 r, SumoUY_UInt8 g, SumoUY_UInt8 b)
{
	SDL_Color textcolor;
	SDL_Rect textarea;
	SDL_Surface *fontsurface;

	//Set the color
	textcolor.r = r;
	textcolor.g = g;
	textcolor.b = b;

	//Render text to surface
	fontsurface = TTF_RenderText_Blended(font,text,textcolor);

	//Create destination rectangle
	textarea.x = x;
    textarea.y = y;
    textarea.w = fontsurface->w;
    textarea.h = fontsurface->h;

	//Blit the surface to the screen surface
	if(SDL_BlitSurface(fontsurface,NULL,screen,&textarea) == -1)
	{
		return SUMOUY_FALSE;
	}

	//Free the font surface, this is important
	SDL_FreeSurface(fontsurface);

	return SUMOUY_TRUE;
}

SumoUY_UInt8 SDL_DrawBot(struct Vector2 globalPosition, SumoUY_Int32 globalRadius, struct Vector2 globalCenter, SumoUY_Int32 localRadius, struct Vector2 localCenter, SumoUY_UInt16 rotation, SumoUY_Int8 opponent)
{
	SDL_Surface *surface;
	SDL_Rect area;

	//Reposition calculus:
	//
	//local(L) = (Lc - Lr) + (((((G - (Gc - Gr) * 100) / (Gr * 2)) * Lr * 2) / 100)
	//
	SumoUY_Int32 localX = (localCenter.X - localRadius) + (((((globalPosition.Y - (globalCenter.Y - globalRadius)) * 100) / (globalRadius * 2)) * localRadius * 2) / 100);
	SumoUY_Int32 localY = (localCenter.Y - localRadius) + (((((globalPosition.X - (globalCenter.X - globalRadius)) * 100) / (globalRadius * 2)) * localRadius * 2) / 100);

	//Rotates the bot surface to the global rotation
	if(actualrotation != previousrotation)
	{
		if(opponent == SUMOUY_FALSE)
		{
			SDL_FreeSurface(newmainbot);
			newmainbot = rotozoomSurface(mainbot, (double)rotation, 1.0, SMOOTHING_ON);
		}
		else
		{
			SDL_FreeSurface(newopponentbot);
			newopponentbot = rotozoomSurface(opponentbot, (double)rotation, 1.0, SMOOTHING_ON);
		}

		//Checks if rotation was done
		if(mainbot == NULL || opponentbot == NULL)
		{
			return SUMOUY_FALSE;
        }

		if(opponent == SUMOUY_FALSE)
		{
			surface = newmainbot;
		}
		else
		{
			surface = newopponentbot;
		}
	}
	else
	{
		if(opponent == SUMOUY_FALSE)
        {
            surface = newmainbot;
        }
        else
        {
            surface = newopponentbot;
        }
	}

	//Creates the area for drawing
	area.x = localX - (surface->w / 2);
	area.y = localY - (surface->h / 2);
	area.w = localX + (surface->w / 2);
	area.h = localY + (surface->h / 2);

	//Draws bot to screen surface
	if(SDL_BlitSurface(surface, NULL, screen, &area) == -1)
	{
		return SUMOUY_FALSE;
	}

	return SUMOUY_TRUE;
}

SumoUY_UInt8 SDL_DrawLogo()
{
	SDL_Rect area;

	//Creates the area for drawing
	area.x = 0;
	area.y = screen->h - logo->h;
	area.w = logo->w;
	area.h = logo->h;


	//Draws logo to screen surface
	if(SDL_BlitSurface(logo,NULL,screen,&area) == -1)
	{
		return SUMOUY_FALSE;
	}

	return SUMOUY_TRUE;
}

SDL_Surface *SDL_CreateDohyo(SumoUY_UInt32 radius)
{
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    SDL_Surface *surface =  SDL_CreateRGBSurface(SDL_HWSURFACE,radius * 2, radius * 2, 32, 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff);
#else
    SDL_Surface *surface =  SDL_CreateRGBSurface(SDL_HWSURFACE,radius * 2, radius * 2, 32, 0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000);
#endif
	SDL_Surface *dohyosurface;

	if(surface == NULL)
	{
		Debug_Write("Can't create dohyo surface");
		return NULL;
	}

	//Draw dohyo elements
	SDL_FillRect(surface,NULL,SDL_MapRGBA(surface->format, 0, 0, 0, 0));
	filledCircleRGBA(surface, radius, radius, radius, 21, 23, 99, 255);
    aacircleRGBA(surface, radius, radius, radius, 0, 0, 111, 255);

	dohyosurface = SDL_DisplayFormatAlpha(surface);

	if(dohyosurface == NULL)
	{
		SDL_FreeSurface(surface);
		Debug_Write("Can't create dohyo surface");
        return NULL;
	}

	SDL_FreeSurface(surface);

	return dohyosurface;
}

SDL_Surface *SDL_CreateBot(SumoUY_UInt32 width, SumoUY_UInt32 height, SumoUY_Int8 opponent)
{
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	SDL_Surface *surface = SDL_CreateRGBSurface(SDL_HWSURFACE, width, height, 32, 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff);
#else
	SDL_Surface *surface = SDL_CreateRGBSurface(SDL_HWSURFACE, width, height, 32, 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff);
#endif
	SDL_Surface *botsurface;

	//Draws bot elements
	if(surface == NULL)
	{
		Debug_Write("Cant create surface to create bot");
		return NULL;
	}

	if(opponent == SUMOUY_FALSE)
	{
		boxRGBA(surface, 0, 0, width, height, 255, 0, 0, 255);
	}
	else
	{
		boxRGBA(surface, 0, 0, width, height, 255, 255, 0, 255);
	}

	botsurface = SDL_DisplayFormatAlpha(surface);

    if(botsurface == NULL)
    {
            SDL_FreeSurface(surface);
            Debug_Write("Cant create surface to create bot");
            return NULL;
    }

    SDL_FreeSurface(surface);

	return botsurface;
}

SDL_Surface *SDL_CreateLogo(char *fileName)
{
	//Load image from file
	SDL_Surface *surface;
	SDL_Surface *logosurface;

    //Check the existence of the image
	if(g_file_test(fileName, G_FILE_TEST_EXISTS) == SUMOUY_FALSE)
	{
	    Debug_Write("The logo file doesn't exist");
		return NULL;
	}

	surface = IMG_Load(fileName);

	//Checks if surface was loaded
	if(surface == NULL)
	{
		Debug_Write("Cant create surface to create logo");
		return NULL;
	}

	//
	//Image resizing formula:
	//
	//size = ((((h/4) - Ih) * 100) / Ih) / 100
	//
	logosurface = zoomSurface(surface, 1.0 + (((((screen->h / 4.0) - surface->h) * 100.0) / surface->h) / 100.0), 1.0 + (((((screen->h / 4.0) - surface->h) * 100.0) / surface->h) / 100.0), SMOOTHING_ON);

	//Checks if surface was loaded
	if(logosurface == NULL)
	{
		Debug_Write("Cant create surface to create logo");
		SDL_FreeSurface(surface);
		return NULL;
	}

	SDL_FreeSurface(surface);

	surface = SDL_DisplayFormatAlpha(logosurface);

	if(surface == NULL)
	{
		Debug_Write("Cant create surface to create logo.");
        SDL_FreeSurface(logosurface);
        return NULL;
	}

	SDL_FreeSurface(logosurface);

	return surface;
}

SumoUY_UInt8 SDL_CompareSpeeds(struct BotSpeed a, struct BotSpeed b)
{
    if((a.LeftWheelSpeed == b.LeftWheelSpeed) && (a.RightWheelSpeed == b.RightWheelSpeed))
    {
        return SUMOUY_TRUE;
    }

    return SUMOUY_FALSE;
}
#endif
