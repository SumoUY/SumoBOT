/*
    Sumo|BOT - Client program for use in the Sumo|UY robotic fight events.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Sumo|BOT.

    Sumo|BOT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Sumo|BOT (2009 - Steven Rodriguez -)                                 //
//=====================================================================//


//===========================================================//
//Libraries                                                  //
//===========================================================//

#include "ai.h"

//===========================================================//
//Internal variables                                         //
//===========================================================//

static GModule *module;
static struct AIModule *aimodule;
static SumoUY_UInt8 loadedmodule = SUMOUY_FALSE;

//===========================================================//
//Functions                                                  //
//===========================================================//

SumoUY_UInt8 AI_LoadModule(char *fileName, struct SumoBot *bot, struct Vector2 dohyoCenter, SumoUY_UInt32 dohyoRadius, void (*onMessage)(char *message), SumoUY_UInt8 aiActive)
{
    //Check if the parameters are valid
    if((fileName == NULL) || (bot == NULL) || (onMessage == NULL))
    {
        return SUMOUY_FALSE;
    }

    //Check if file exists
    if(g_file_test(fileName, G_FILE_TEST_EXISTS) == FALSE)
    {
        return SUMOUY_FALSE;
    }

    //Load module and check if loaded
    module = g_module_open(fileName, G_MODULE_BIND_LAZY);

    if(module == NULL)
    {
        return SUMOUY_FALSE;
    }

    //Get the module from file
    struct AIModule *(*GetModule)();

    if (g_module_symbol(module,"GetModule", (gpointer *)&GetModule) == FALSE)
    {
        g_module_close(module);
        return SUMOUY_FALSE;
    }

    aimodule = GetModule();

    if(aimodule == NULL)
    {
        return SUMOUY_FALSE;
    }

    loadedmodule = SUMOUY_TRUE;

    //Load module
    if(aimodule->Load(bot, dohyoCenter, dohyoRadius, onMessage, aiActive) == SUMOUY_FALSE)
    {
        return SUMOUY_FALSE;
    }

    return SUMOUY_TRUE;
}

SumoUY_UInt8 AI_UnloadModule()
{
    if(loadedmodule == SUMOUY_TRUE)
    {
        aimodule = NULL;
        g_module_close(module);
        loadedmodule = SUMOUY_FALSE;

        return SUMOUY_TRUE;
    }

    return SUMOUY_FALSE;
}

char *AI_GetModuleName()
{
    if(loadedmodule == SUMOUY_TRUE)
    {
        return aimodule->Name;
    }

    return NULL;
}

char *AI_GetModuleVersion()
{
    if(loadedmodule == SUMOUY_TRUE)
    {
        return aimodule->Version;
    }

    return NULL;
}

char *AI_GetModuleVendor()
{
    if(loadedmodule == SUMOUY_TRUE)
    {
        return aimodule->Vendor;
    }

    return NULL;
}

void AI_ProcessStep()
{
    if(loadedmodule == SUMOUY_TRUE)
    {
        aimodule->ProcessStep();
    }
}

void AI_SendAIToggleEvent(SumoUY_UInt8 activated)
{
    if(loadedmodule == SUMOUY_TRUE)
    {
        aimodule->OnSumoBotAIToggle(activated);
    }
}

SumoUY_UInt8 AI_IsLoaded()
{
    return loadedmodule;
}
