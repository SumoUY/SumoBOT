/*
    Sumo|BOT - Client program for use in the Sumo|UY robotic fight events.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Sumo|BOT.

    Sumo|BOT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Sumo|BOT (2009 - Steven Rodriguez -)                                 //
//=====================================================================//

////////////////////////////////////////////////////////////////////
/// \file sdlinterface.h
/// \brief Sumo|BOT SDL interface file.
/// \details This is the SDL interface of the Sumo|BOT program.
////////////////////////////////////////////////////////////////////

//===========================================================//
//Libraries                                                  //
//===========================================================//

#if !defined(SUMOBOTHEADER)

#include "sumobot.h"

#endif

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_gfxPrimitives.h>
#include <SDL_rotozoom.h>
#include <SDL_ttf.h>

//===========================================================//
//Functions                                                  //
//===========================================================//

////////////////////////////////////////////////////
/// \fn void SDL_LoadInterface(struct SumoBot *bot, struct Vector2 dohyoCenter, SumoUY_UInt32 dohyoRadius, SumoUY_UInt32 fps, SumoUY_Int8 fullscreen, SumoUY_UInt32 width, SumoUY_UInt32 height, SumoUY_UInt8 bpp, SumoUY_UInt8 useJoystick)
/// \brief Loads the SDL interface.
/// \param bot The SumoBot to use.
/// \param dohyoCenter The center of the dohyo.
/// \param dohyoRadius The radius of the dohyo.
/// \param fps The FPS wish.
/// \param fullscreen SUMOUY_TRUE if fullscreen, SUMOUY_FALSE if not.
/// \param width The screen width in pixels.
/// \param height The screen height in pixels.
/// \param useJoystick SUMOUY_TRUE to use joystick, SUMOUY_FALSE to not use it.
////////////////////////////////////////////////////
void SDL_LoadInterface(struct SumoBot *bot, struct Vector2 dohyoCenter, SumoUY_UInt32 dohyoRadius, SumoUY_UInt32 fps, SumoUY_Int8 fullscreen, SumoUY_UInt32 width, SumoUY_UInt32 height, SumoUY_UInt8 bpp, SumoUY_UInt8 useJoystick);

////////////////////////////////////////////////////
/// \fn void SDL_CheckInput()
/// \brief Checks the input.
////////////////////////////////////////////////////
void SDL_CheckInput();

////////////////////////////////////////////////////
/// \fn void SDL_ShowFrame()
/// \brief Draws a frame.
////////////////////////////////////////////////////
void SDL_ShowFrame();

////////////////////////////////////////////////////
/// \fn void SDL_Debug(char *message)
/// \brief Writes a message in the debug system.
/// \param message The message to write.
////////////////////////////////////////////////////
void SDL_Debug(char *message);

////////////////////////////////////////////////////
/// \fn void SDL_CheckSumoLIBError()
/// \brief Checks for Sumo|LIB errors.
////////////////////////////////////////////////////
void SDL_CheckSumoLIBError();

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SDL_DrawDohyo(SumoUY_Int32 x, SumoUY_Int32 y)
/// \brief Draws the dohyo to the screen.
/// \param x The x position to draw.
/// \param y The y position to draw.
/// \return SUMOUY_TRUE if done or SUMOUY_FALSE if can't do the operation.
////////////////////////////////////////////////////
SumoUY_UInt8 SDL_DrawDohyo(SumoUY_Int32 x, SumoUY_Int32 y);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SDL_DrawText(char *text,SumoUY_UInt32 x, SumoUY_UInt32 y, SumoUY_UInt8 r, SumoUY_UInt8 g, SumoUY_UInt8 b)
/// \brief Draws specified text in screen.
/// \param x The x position to draw.
/// \param y The y position to draw.
/// \param r The red color value (0 to 255).
/// \param g The green color value (0 to 255).
/// \param b The blue color value (0 to 255).
/// \return SUMOUY_TRUE if done or SUMOUY_FALSE if can't do the operation.
////////////////////////////////////////////////////
SumoUY_UInt8 SDL_DrawText(char *text,SumoUY_UInt32 x, SumoUY_UInt32 y, SumoUY_UInt8 r, SumoUY_UInt8 g, SumoUY_UInt8 b);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SDL_DrawBot(struct Vector2 globalPosition, SumoUY_Int32 globalRadius, struct Vector2 globalCenter, SumoUY_Int32 localRadius, struct Vector2 localCenter, SumoUY_UInt16 rotation, SumoUY_Int8 opponent)
/// \brief Draws specified bot in screen.
/// \param globalPosition The global position of the bot.
/// \param globalRadius The global radius of the dohyo.
/// \param globalCenter The global center of the dohyo
/// \param localRadius The local radius of the dohyo.
/// \param localCenter The local center of the dohyo.
/// \param rotation The rotation in degrees.
/// \param opponent SUMOUY_TRUE to draw opponent bot and SUMOUY_FALSE to draw bot.
/// \return SUMOUY_TRUE if done or SUMOUY_FALSE if can't do the operation.
////////////////////////////////////////////////////
SumoUY_UInt8 SDL_DrawBot(struct Vector2 globalPosition, SumoUY_Int32 globalRadius, struct Vector2 globalCenter, SumoUY_Int32 localRadius, struct Vector2 localCenter, SumoUY_UInt16 rotation, SumoUY_Int8 opponent);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SDL_DrawLogo()
/// \brief Draws the logo.
/// \return SUMOUY_TRUE if done or SUMOUY_FALSE if can't do the operation.
////////////////////////////////////////////////////
SumoUY_UInt8 SDL_DrawLogo();

////////////////////////////////////////////////////
/// \fn SDL_Surface *SDL_CreateDohyo(SumoUY_UInt32 radius)
/// \brief Creates a dohyo surface.
/// \param radius The dohyo radius.
/// \return The dohyo surface or NULL on failure.
////////////////////////////////////////////////////
SDL_Surface *SDL_CreateDohyo(SumoUY_UInt32 radius);

////////////////////////////////////////////////////
/// \fn SDL_Surface *SDL_CreateBot(SumoUY_UInt32 width, SumoUY_UInt32 height, SumoUY_Int8 opponent)
/// \brief Creates a bot surface .
/// \param width Width of the bot in pixels.
/// \param height Height of the bot in pixels.
/// \param opponent SUMOUY_TRUE to draw opponent bot and SUMOUY_FALSE to draw bot.
/// \return The bot surface or NULL on failure.
////////////////////////////////////////////////////
SDL_Surface *SDL_CreateBot(SumoUY_UInt32 width, SumoUY_UInt32 height, SumoUY_Int8 opponent);

////////////////////////////////////////////////////
/// \fn SDL_Surface *SDL_CreateLogo(char *fileName)
/// \brief Creates the logo.
/// \param fileName The logo filename to load.
/// \return The logo surface or NULL on failure.
////////////////////////////////////////////////////
SDL_Surface *SDL_CreateLogo(char *fileName);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SDL_CompareSpeeds(struct BotSpeed a, struct BotSpeed b)
/// \brief Compares two BotSpeeds.
/// \param a A BotSpeed to compare.
/// \param b A BotSpeed to compare.
/// \return SUMOUY_TRUE if are equals, SUMOUY_FALSE if not.
////////////////////////////////////////////////////
SumoUY_UInt8 SDL_CompareSpeeds(struct BotSpeed a, struct BotSpeed b);
