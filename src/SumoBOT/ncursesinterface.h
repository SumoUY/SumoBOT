/*
    Sumo|BOT - Client program for use in the Sumo|UY robotic fight events.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Sumo|BOT.

    Sumo|BOT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Sumo|BOT (2009 - Steven Rodriguez -)                                 //
//=====================================================================//

////////////////////////////////////////////////////////////////////
/// \file ncursesinterface.h
/// \brief Sumo|BOT NCurses interface file.
/// \details This is the NCurses interface of the Sumo|BOT program.
////////////////////////////////////////////////////////////////////

//===========================================================//
//Libraries                                                  //
//===========================================================//

#if !defined(SUMOBOTHEADER)

#include "sumobot.h"

#endif

#include <ncurses.h>

//===========================================================//
//Functions                                                  //
//===========================================================//

////////////////////////////////////////////////////
/// \fn void NCurses_LoadInterface(struct SumoBot *bot, struct Vector2 dohyoCenter, SumoUY_UInt32 dohyoRadius, SumoUY_UInt32 fps, SumoUY_Int8 fullscreen, SumoUY_UInt32 width, SumoUY_UInt32 height, SumoUY_UInt8 bpp, SumoUY_UInt8 useJoystick)
/// \brief Loads the NCurses interface.
/// \param bot The SumoBot to use.
/// \param dohyoCenter The center of the dohyo.
/// \param dohyoRadius The radius of the dohyo.
/// \param fps The FPS wish.
/// \param fullscreen SUMOUY_TRUE if fullscreen, SUMOUY_FALSE if not.
/// \param width The screen width in pixels.
/// \param height The screen height in pixels.
/// \param useJoystick SUMOUY_TRUE to use joystick, SUMOUY_FALSE to not use it.
////////////////////////////////////////////////////
void NCurses_LoadInterface(struct SumoBot *bot, struct Vector2 dohyoCenter, SumoUY_UInt32 dohyoRadius, SumoUY_UInt32 fps, SumoUY_Int8 fullscreen, SumoUY_UInt32 width, SumoUY_UInt32 height, SumoUY_UInt8 bpp, SumoUY_UInt8 useJoystick);

////////////////////////////////////////////////////
/// \fn void NCurses_CheckInput()
/// \brief Checks the input.
////////////////////////////////////////////////////
void NCurses_CheckInput();

////////////////////////////////////////////////////
/// \fn void NCurses_ShowFrame()
/// \brief Draws a frame.
////////////////////////////////////////////////////
void NCurses_ShowFrame();

////////////////////////////////////////////////////
/// \fn void NCurses_Debug(char *message)
/// \brief Writes a message in the debug system.
/// \param message The message to write.
////////////////////////////////////////////////////
void NCurses_Debug(char *message);

////////////////////////////////////////////////////
/// \fn void NCurses_CheckSumoLIBError()
/// \brief Checks for Sumo|LIB errors.
////////////////////////////////////////////////////
void NCurses_CheckSumoLIBError();

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 NCurses_CompareSpeeds(struct BotSpeed a, struct BotSpeed b)
/// \brief Compares two BotSpeeds.
/// \param a A BotSpeed to compare.
/// \param b A BotSpeed to compare.
/// \return SUMOUY_TRUE if are equals, SUMOUY_FALSE if not.
////////////////////////////////////////////////////
SumoUY_UInt8 NCurses_CompareSpeeds(struct BotSpeed a, struct BotSpeed b);
