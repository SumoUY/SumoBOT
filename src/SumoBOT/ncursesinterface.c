/*
    Sumo|BOT - Client program for use in the Sumo|UY robotic fight events.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Sumo|BOT.

    Sumo|BOT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Sumo|BOT (2009 - Steven Rodriguez -)                                 //
//=====================================================================//
#if defined(SUMOBOT_UI_NCURSES)
//=========================================//
//Libraries                                //
//=========================================//

#include "ncursesinterface.h"

//=========================================//
//Internal variables                       //
//=========================================//

//Console specific
WINDOW *consolewin;
GString *text;

//UI specific
static SumoUY_UInt32 framespersecond;
static SumoUY_UInt8 finish;
static SumoUY_UInt8 hasjoystick;
static SDL_Joystick *joystick;
static SumoUY_Int8 currentspeed;
static SumoUY_UInt8 ai;

//Fps calculation
static SumoUY_UInt32 frametime;
static SumoUY_UInt32 starttime;
static SumoUY_UInt32 endtime;
static SumoUY_Int32 totaltime;
static SumoUY_UInt32 actualfps;

//Debug specific
static GString *debugstring;

//Sumo|LIB specific
static struct SumoBot *sumobot;
static struct Vector2 dcenter;
static SumoUY_UInt32 dradius;
static struct BotSpeed lastspeed;

//=========================================//
//Functions                                //
//=========================================//

void NCurses_LoadInterface(struct SumoBot *bot, struct Vector2 dohyoCenter, SumoUY_UInt32 dohyoRadius, SumoUY_UInt32 fps, SumoUY_Int8 fullscreen, SumoUY_UInt32 width, SumoUY_UInt32 height, SumoUY_UInt8 bpp, SumoUY_UInt8 useJoystick)
{
    //Initialize variables
    debugstring = g_string_new(NULL);
    currentspeed = 0;
    ai = SUMOUY_FALSE;
    lastspeed = SumoUY_CreateBotSpeed(0, 0);

    //Obtain variables
    sumobot = bot;
    dcenter = dohyoCenter;
    dradius = dohyoRadius;
    framespersecond = fps;
    hasjoystick = useJoystick;

    if(hasjoystick == SUMOUY_TRUE)
	{
	    //Initialize joystick and timer
		if(SDL_Init(SDL_INIT_JOYSTICK | SDL_INIT_TIMER) == -1)
		{
			Debug_Write("Could not init joystick");
			g_string_free(debugstring, TRUE);
			return;
		}

        //Check if number of joysticks is >= 1
		if(SDL_NumJoysticks() == 0)
		{
			Debug_Write("Joystick not found. Please, connect a joystick");
			g_string_free(debugstring, TRUE);
			return;
		}

        //Open joystick
		joystick = SDL_JoystickOpen(0);

		if(joystick == NULL)
		{
			Debug_Write("Can't open joystick");
			g_string_free(debugstring, TRUE);
			return;
		}

		//Select joystick to update manually
		SDL_JoystickEventState(SDL_IGNORE);
	}
	else
	{
	    //Initialize timer
		if(SDL_Init(SDL_INIT_TIMER) == -1)
		{
			Debug_Write("Could not init timer");
			g_string_free(debugstring, TRUE);
			return;
		}
	}

    //Initialize NCurses
    consolewin = initscr();

    if(consolewin == NULL)
    {
        Debug_Write("Could not init Ncurses");
        g_string_free(debugstring, TRUE);
        return;
    }

    //Check for columns and rows specified
	if(COLS <= 85 && LINES <= 25)
	{
		endwin();
		while(isendwin() == FALSE);
		Debug_Write("You must to have at least 85 columns and 25 rows!");
        g_string_free(debugstring, TRUE);
		return;
	}

    //Special keypad function
	if(keypad(consolewin,TRUE) != OK)
	{
		Debug_Write("Can't select keypad. Prepare for hell...");
	}

    //Special nodelay function
	if(nodelay(consolewin,TRUE) != OK)
    {
        Debug_Write("Can't select nodelay. Prepare for hell...");
    }

    //Hide the cursor
	if(curs_set(0) == ERR)
	{
		Debug_Write("Can't hide cursor");
	}

	//Start color system
	if(start_color() == ERR)
	{
		Debug_Write("Can't start console coloring system");
	}

	//Initialize text
	text = g_string_new(NULL);

	//Change debug function
    Debug_SetEvent(NCurses_Debug);

    //Initialize the frame limiter
    frametime = 1000 / framespersecond;

    //Initialize Bot
    SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(0, 0));

    //NCurses main loop
    finish = SUMOUY_FALSE;

    while(finish == SUMOUY_FALSE)
    {
        //----------------------------
		//Frame limiter function
		starttime = SDL_GetTicks();
		//----------------------------

        //Check for errors
        if(SumoUY_GetError() != SUMOUY_NO_ERROR)
        {
                Debug_Write(SumoUY_GetErrorString());
                break;
        }

        //Check input
        NCurses_CheckInput();

        //Process AI or update the bot state
        if (ai == SUMOUY_FALSE)
        {
            if(SumoUY_BotProcessData(bot) == SUMOUY_FALSE)
            {
                Debug_Write(SumoUY_GetErrorString());
            }
        }
        else
        {
            AI_ProcessStep();
        }

        //Show Frame
        NCurses_ShowFrame();

        //----------------------------
        //Frame limiter function
        endtime = SDL_GetTicks();

        totaltime = frametime - (endtime - starttime);

		if(totaltime > 0)
		{
			SDL_Delay(frametime - (endtime - starttime));
		}

		endtime = SDL_GetTicks();
		actualfps = 1000 / (endtime - starttime);
        //----------------------------
    }

    //Unload NCurses, text and joystick
    g_string_free(text, TRUE);
    endwin();

    while(isendwin() == FALSE);

    if(hasjoystick == SUMOUY_TRUE)
    {
        if(joystick != NULL)
		{
			SDL_JoystickClose(joystick);
		}
    }
}

void NCurses_CheckInput()
{
    if(hasjoystick == SUMOUY_TRUE) //Check joystick input
    {
        //Poll joystick status
		SDL_JoystickUpdate();

		//Check QUIT action
		if(SDL_JoystickGetButton(joystick,8) == 1)
		{
			finish = SUMOUY_TRUE;
            return;
		}

		//Check AI TOGGLE action
        if(SDL_JoystickGetButton(joystick,6) == 1)
        {
            if(AI_IsLoaded() == SUMOUY_TRUE)
            {
                if(ai == SUMOUY_TRUE)
                {
                    ai = SUMOUY_FALSE;
                }
                else
                {
                    ai = SUMOUY_TRUE;
                }
                AI_SendAIToggleEvent(ai);
            }
            return;
        }

		if(ai == SUMOUY_FALSE)
		{

            //Check SPEED 0 action
            if(SDL_JoystickGetButton(joystick,0) == 1)
            {
                currentspeed = 0;
                return;
            }

            //Check SPEED 1 action
            if(SDL_JoystickGetButton(joystick,1) == 1)
            {
                currentspeed = 1;
                return;
            }

            //Check SPEED 2 action
            if(SDL_JoystickGetButton(joystick,2) == 1)
            {
                currentspeed = 2;
                return;
            }

            //Check SPEED 3 action
            if(SDL_JoystickGetButton(joystick,3) == 1)
            {
                currentspeed = 3;
                return;
            }

            //Check SPEED 4 action
            if(SDL_JoystickGetButton(joystick,4) == 1)
            {
                currentspeed = 4;
                return;
            }

            //Check SPEED 5 action
            if(SDL_JoystickGetButton(joystick,5) == 1)
            {
                currentspeed = 5;
                return;
            }

            //Check STOP action
            if(SDL_JoystickGetAxis(joystick,0) == 0 && SDL_JoystickGetAxis(joystick,1) == 0)
            {
                if(NCurses_CompareSpeeds(lastspeed, SumoUY_CreateBotSpeed(0, 0)) == SUMOUY_FALSE)
                {
                    SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(0, 0));
                    lastspeed = SumoUY_CreateBotSpeed(0, 0);
                }
                NCurses_CheckSumoLIBError();
                return;
            }

            //Check RIGHT action
            if(SDL_JoystickGetAxis(joystick,0) > 0)
            {
                if(NCurses_CompareSpeeds(lastspeed, SumoUY_CreateBotSpeed(currentspeed, - currentspeed)) == SUMOUY_FALSE)
                {
                    SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(currentspeed, - currentspeed));
                    lastspeed = SumoUY_CreateBotSpeed(currentspeed, - currentspeed);
                }
                NCurses_CheckSumoLIBError();
                return;
            }

            //Check LEFT action
            if(SDL_JoystickGetAxis(joystick,0) < 0)
            {
                if(NCurses_CompareSpeeds(lastspeed, SumoUY_CreateBotSpeed(- currentspeed, currentspeed)) == SUMOUY_FALSE)
                {
                    SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(- currentspeed, currentspeed));
                    lastspeed = SumoUY_CreateBotSpeed(- currentspeed, currentspeed);
                }
                NCurses_CheckSumoLIBError();
                return;
            }

            //Check UP action
            if(SDL_JoystickGetAxis(joystick,1) < 0)
            {
                if(NCurses_CompareSpeeds(lastspeed, SumoUY_CreateBotSpeed(currentspeed, currentspeed)) == SUMOUY_FALSE)
                {
                    SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(currentspeed, currentspeed));
                    lastspeed = SumoUY_CreateBotSpeed(currentspeed, currentspeed);
                }
                NCurses_CheckSumoLIBError();
                return;
            }

            //Check DOWN action
            if(SDL_JoystickGetAxis(joystick,1) > 0)
            {
                if(NCurses_CompareSpeeds(lastspeed, SumoUY_CreateBotSpeed(- currentspeed, - currentspeed)) == SUMOUY_FALSE)
                {
                    SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(- currentspeed, - currentspeed));
                    lastspeed = SumoUY_CreateBotSpeed(- currentspeed, - currentspeed);
                }
                NCurses_CheckSumoLIBError();
                return;
            }
		}
    }
    else //Check keyboard input
    {
        switch(getch())
        {
            case 'q': //Check QUIT action
                finish = SUMOUY_TRUE;
                return;
            case 'a': //Check AI_TOGGLE action
                if(AI_IsLoaded() == SUMOUY_TRUE)
                {
                    if(ai == SUMOUY_TRUE)
                    {
                        ai = SUMOUY_FALSE;
                    }
                    else
                    {
                        ai = SUMOUY_TRUE;
                    }
                    AI_SendAIToggleEvent(ai);
                }
                return;
        }

        if(ai == SUMOUY_FALSE)
        {
            switch(getch())
            {
                case KEY_UP: //Check UP action
                    if(NCurses_CompareSpeeds(lastspeed, SumoUY_CreateBotSpeed(currentspeed, currentspeed)) == SUMOUY_FALSE)
                    {
                        SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(currentspeed, currentspeed));
                        lastspeed = SumoUY_CreateBotSpeed(currentspeed, currentspeed);
                    }
                    NCurses_CheckSumoLIBError();
                    return;
                case KEY_DOWN: //Check DOWN action
                    if(NCurses_CompareSpeeds(lastspeed, SumoUY_CreateBotSpeed(- currentspeed, - currentspeed)) == SUMOUY_FALSE)
                    {
                        SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(- currentspeed, - currentspeed));
                        lastspeed = SumoUY_CreateBotSpeed(- currentspeed, - currentspeed);
                    }
                    NCurses_CheckSumoLIBError();
                    return;
                case KEY_LEFT: //Check LEFT action
                    if(NCurses_CompareSpeeds(lastspeed, SumoUY_CreateBotSpeed(- currentspeed, currentspeed)) == SUMOUY_FALSE)
                    {
                        SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(- currentspeed, currentspeed));
                        lastspeed = SumoUY_CreateBotSpeed(- currentspeed, currentspeed);
                    }
                    NCurses_CheckSumoLIBError();
                    return;
                case KEY_RIGHT: //Check RIGHT action
                    if(NCurses_CompareSpeeds(lastspeed, SumoUY_CreateBotSpeed(currentspeed, - currentspeed)) == SUMOUY_FALSE)
                    {
                        SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(currentspeed, - currentspeed));
                        lastspeed = SumoUY_CreateBotSpeed(currentspeed, - currentspeed);
                    }
                    NCurses_CheckSumoLIBError();
                    return;
                case '0': //Check SPEED 0 action
                    currentspeed = 0;
                    return;
                case '1': //Check SPEED 1 action
                    currentspeed = 1;
                    return;
                case '2': //Check SPEED 2 action
                    currentspeed = 2;
                    return;
                case '3': //Check SPEED 3 action
                    currentspeed = 3;
                    return;
                case '4': //Check SPEED 4 action
                    currentspeed = 4;
                    return;
                case '5': //Check SPEED 5 action
                    currentspeed = 5;
                    return;
                default: //Check STOP action
                    if(NCurses_CompareSpeeds(lastspeed, SumoUY_CreateBotSpeed(0, 0)) == SUMOUY_FALSE)
                    {
                        SumoUY_BotSetSpeed(sumobot, SumoUY_CreateBotSpeed(0, 0));
                        lastspeed = SumoUY_CreateBotSpeed(0, 0);
                    }
                    NCurses_CheckSumoLIBError();
                    return;
            }
        }
    }
}

void NCurses_ShowFrame()
{
    //Clears the screen
	erase();

	//Initialize the yellow color
	if(has_colors() == TRUE)
	{
		init_pair(1,COLOR_YELLOW,COLOR_BLACK);
		attron(COLOR_PAIR(1) | A_BOLD);
	}

	//Draw FPS
	g_string_printf(text, "FPS: %i", actualfps);
	mvprintw(0, 0, text->str);

	//Draw position
	g_string_printf(text, "Position: X:%i Y:%i", SumoUY_BotGetPosition(sumobot).X, SumoUY_BotGetPosition(sumobot).Y);
	NCurses_CheckSumoLIBError();
    mvprintw(1, 0, text->str);

	//Draw rotation
	g_string_printf(text, "Rotation: %hu", SumoUY_BotGetRotation(sumobot));
	NCurses_CheckSumoLIBError();
    mvprintw(2, 0, text->str);

	//Draw yukos
    g_string_printf(text, "Yukos: %hhu", SumoUY_BotGetYukos(sumobot));
	NCurses_CheckSumoLIBError();
	mvprintw(3, 0, text->str);

	//Draw opponent position
	g_string_printf(text, "Opponent position: X:%i Y:%i", SumoUY_BotGetOpponentPosition(sumobot).X, SumoUY_BotGetOpponentPosition(sumobot).Y);
	NCurses_CheckSumoLIBError();
    mvprintw(4, 0, text->str);

	//Draw opponent rotation
	g_string_printf(text, "Opponent rotation: %hu", SumoUY_BotGetOpponentRotation(sumobot));
	NCurses_CheckSumoLIBError();
	mvprintw(5, 0, text->str);

	//Draw opponent yukos
	g_string_printf(text, "Opponent yukos: %hhu", SumoUY_BotGetOpponentYukos(sumobot));
	NCurses_CheckSumoLIBError();
	mvprintw(6, 0, text->str);

	//Draw left wheel speed
	g_string_printf(text, "Left wheel speed: %hhd", SumoUY_BotGetSpeed(sumobot).LeftWheelSpeed);
	NCurses_CheckSumoLIBError();
	mvprintw(7, 0, text->str);

	//Draw right wheel speed
	g_string_printf(text, "Right wheel speed: %hhd", SumoUY_BotGetSpeed(sumobot).RightWheelSpeed);
	NCurses_CheckSumoLIBError();
	mvprintw(8, 0, text->str);

	//Unload the yellow color and load the red color
	if(has_colors() == TRUE)
	{
		attroff(COLOR_PAIR(1) | A_BOLD);
		init_pair(2,COLOR_RED,COLOR_BLACK);
		attron(COLOR_PAIR(2) | A_BOLD);
	}

	//Draw speed
	g_string_printf(text, "Manual speed: %i", currentspeed);
	NCurses_CheckSumoLIBError();
    mvprintw(10, 0, text->str);

	//Unload the red color and load the green color
    if(has_colors() == TRUE)
    {
        attroff(COLOR_PAIR(2) | A_BOLD);
        init_pair(6,COLOR_GREEN,COLOR_BLACK);
        attron(COLOR_PAIR(6) | A_BOLD);
    }

    //Draw AI module
    if(AI_IsLoaded() == SUMOUY_TRUE)
    {
        g_string_printf(text, "AI Module: loaded");
        mvprintw(12, 0, text->str);
        g_string_printf(text, "Name: %s", AI_GetModuleName());
        mvprintw(13, 0, text->str);
        g_string_printf(text, "Version: v%s", AI_GetModuleVersion());
        mvprintw(14, 0, text->str);
        g_string_printf(text, "Vendor: %s", AI_GetModuleVendor());
        mvprintw(15, 0, text->str);
    }
    else
    {
        g_string_printf(text, "AI Module: not loaded");
        mvprintw(12, 0, text->str);
    }


	//Draw AI
	if(ai == SUMOUY_TRUE)
	{
		g_string_printf(text, "AI: active");
	}
	else
	{
		g_string_printf(text, "AI: inactive");
	}
	mvprintw(16, 0, text->str);

	//Draw logo

	//Unload the red color and load the white color
	if(has_colors() == TRUE)
	{
		attroff(COLOR_PAIR(6) | A_BOLD);
		init_pair(3,COLOR_WHITE,COLOR_BLACK);
		attron(COLOR_PAIR(3) | A_BOLD);
	}
	mvprintw(12,(COLS - 29) / 2,"        FULL METAL");

	//Unload the white color and load the red color
	if(has_colors() == TRUE)
	{
		attroff(COLOR_PAIR(3) | A_BOLD);
		init_pair(2,COLOR_RED,COLOR_BLACK);
		attron(COLOR_PAIR(2) | A_BOLD);
	}

	mvprintw(13,(COLS - 29) / 2,"  ____  _   _ __  __  ___");
	mvprintw(14,(COLS - 29) / 2," / ___|| | | |  \\/  |/ _ \\");
	mvprintw(15,(COLS - 29) / 2," \\___ \\| | | | |\\/| | | | |");
	mvprintw(16,(COLS - 29) / 2,"  ___) | |_| | |  | | |_| |");
	mvprintw(17,(COLS - 29) / 2," |____/ \\___/|_|  |_|\\___/");

	//Unload the red color and load the red color
	if(has_colors() == TRUE)
	{
		attroff(COLOR_PAIR(2) | A_BOLD);
		init_pair(4,COLOR_BLUE,COLOR_BLACK);
		attron(COLOR_PAIR(4) | A_BOLD);
	}
	mvprintw(19,(COLS - 29) / 2,"Build the beast - Be the best");

	//Initialize the color
    if(has_colors() == TRUE)
    {
        attroff(COLOR_PAIR(2) | A_BOLD);
        init_pair(5,COLOR_WHITE,COLOR_BLACK);
        attron(COLOR_PAIR(5) | A_BOLD);
    }

    mvprintw(LINES - 2,(COLS - debugstring->len) / 2,debugstring->str);

    //Unload colors
    if(has_colors() == TRUE)
    {
        attroff(COLOR_PAIR(5) | A_BOLD);
    }

	//Swap buffers
	refresh();
}

void NCurses_Debug(char *message)
{
    g_string_printf(debugstring, "%s", message);
}

void NCurses_CheckSumoLIBError()
{
    if(SumoUY_GetError() != SUMOUY_NO_ERROR)
    {
        Debug_Write(SumoUY_GetErrorString());
    }
}

SumoUY_UInt8 NCurses_CompareSpeeds(struct BotSpeed a, struct BotSpeed b)
{
    if((a.LeftWheelSpeed == b.LeftWheelSpeed) && (a.RightWheelSpeed == b.RightWheelSpeed))
    {
        return SUMOUY_TRUE;
    }

    return SUMOUY_FALSE;
}
#endif
